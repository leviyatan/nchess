#include "piece.h"
#include <ncurses.h>

void Piece::move(int new_x, int new_y){
    if(legal_move(new_x, new_y)){
        x = new_x;
        y = new_y;
    }
}

void Piece::draw(){
    start_color();
    use_default_colors();
    init_pair(1, COLOR_BLUE, -1);
    init_pair(2, COLOR_RED, -1);
    
    switch(color){
        case BLACK:
        attron(COLOR_PAIR(1));
        mvprintw(y-1, x-1, "%c", character);
        attroff(COLOR_PAIR(1));
        break;

        case WHITE:
        attron(COLOR_PAIR(2));
        mvprintw(y-1, x-1, "%c", character);
        attroff(COLOR_PAIR(2));
        break;
    }
}

int Piece::get_x(){
    return x;
}

int Piece::get_y(){
    return y;
}

int Piece::get_color(){
    return color;
}

int Piece::get_type(){
    return type;
}

