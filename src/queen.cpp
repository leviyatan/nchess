#include "queen.h"

Queen::Queen(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = QUEEN;
    character = 'Q';
}

bool Queen::legal_move(int new_x, int new_y){
    bool legal = false;

    if(abs(new_x - x) == abs(new_y - y)){
        legal = true;
    }

    if((new_x == x && new_y != y) || (new_y == y && new_x != x)){
        legal = true;
    }

    return legal;
}