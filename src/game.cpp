#include <ncurses.h>
#include <iostream>
#include "game.h"
#include "board.h"
#include "piece.h"

Board* board = new Board;

Game::Game(){
}

void Game::enter_move(){
	const char* mesg = "Enter a move: ";
	mvprintw(9,0,"%s",mesg);
	char str[4];
	getnstr(str, 4);
	parse_move(str);

	if(board->get_piece(str[0], str[1]) != -1){
		int i = board->get_piece(str[0], str[1]);
		board->pieces[i]->move(str[2], str[3]);
	}

	clear();
}

void Game::parse_move(char* str){
	str[0] -= 96;
	str[1] = abs(str[1] - '0' - 9);
	str[2] -= 96;
	str[3] = abs(str[3] - '0' - 9);
}

void Game::start_game(){
	while(true){
		board->draw_board();
		enter_move();
		refresh();
	}
}