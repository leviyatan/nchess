#include "rook.h"

Rook::Rook(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = ROOK;
    character = 'R';
    has_moved = false;
}

bool Rook::legal_move(int new_x, int new_y){
    bool legal = false;

    if((new_x == x && new_y != y) || (new_y == y && new_x != x)){
        legal = true;
        has_moved = true;
    }

    return legal;
}