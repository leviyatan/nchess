#include <ncurses.h>
#include <vector>
#include "board.h"
#include "piece.h"
#include "pawn.h"
#include "knight.h"
#include "bishop.h"
#include "rook.h"
#include "queen.h"
#include "king.h"

Board::Board(){
    pieces.push_back(new Rook(1,1,BLACK));
    pieces.push_back(new Knight(2,1,BLACK));
    pieces.push_back(new Bishop(3,1,BLACK));
    pieces.push_back(new Queen(4,1,BLACK));
    pieces.push_back(new King(5,1,BLACK));
    pieces.push_back(new Bishop(6,1,BLACK));
    pieces.push_back(new Knight(7,1,BLACK));
    pieces.push_back(new Rook(8,1,BLACK));
    pieces.push_back(new Pawn(1,2,BLACK));
    pieces.push_back(new Pawn(2,2,BLACK));
    pieces.push_back(new Pawn(3,2,BLACK));
    pieces.push_back(new Pawn(4,2,BLACK));
    pieces.push_back(new Pawn(5,2,BLACK));
    pieces.push_back(new Pawn(6,2,BLACK));
    pieces.push_back(new Pawn(7,2,BLACK));
    pieces.push_back(new Pawn(8,2,BLACK));

    pieces.push_back(new Rook(1,8,WHITE));
    pieces.push_back(new Knight(2,8,WHITE));
    pieces.push_back(new Bishop(3,8,WHITE));
    pieces.push_back(new Queen(4,8,WHITE));
    pieces.push_back(new King(5,8,WHITE));
    pieces.push_back(new Bishop(6,8,WHITE));
    pieces.push_back(new Knight(7,8,WHITE));
    pieces.push_back(new Rook(8,8,WHITE));
    pieces.push_back(new Pawn(1,7,WHITE));
    pieces.push_back(new Pawn(2,7,WHITE));
    pieces.push_back(new Pawn(3,7,WHITE));
    pieces.push_back(new Pawn(4,7,WHITE));
    pieces.push_back(new Pawn(5,7,WHITE));
    pieces.push_back(new Pawn(6,7,WHITE));
    pieces.push_back(new Pawn(7,7,WHITE));
    pieces.push_back(new Pawn(8,7,WHITE));
}

void Board::draw_board(){
    bool white_square = true;

    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if(white_square){
                mvprintw(i, j, "0");
                white_square = false;
            } else {
                mvprintw(i, j, "#");
                white_square = true;
            }
        }
        white_square = !white_square;
    }

    for(int i = 0; i < pieces.size(); i++){
        pieces[i]->draw();
    }
}

int Board::get_piece(int x, int y){
    for(int i = 0; i < pieces.size(); i++){
        if(pieces[i]->get_x() == x && pieces[i]->get_y() == y){
            return i;
        }
    }
    return -1;
}