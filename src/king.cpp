#include "king.h"

King::King(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = KING;
    character = 'K';
    has_moved = false;
}

bool King::legal_move(int new_x, int new_y){
    bool legal = false;
    
    if((new_x >= x-1 && new_x <= x+1) && (new_y >= y-1 && new_y <= y+1)){
        legal = true;
        has_moved = true;
    }

    return legal;
}