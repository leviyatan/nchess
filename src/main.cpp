#include <ncurses.h>
#include "game.h"

int main(){
	initscr();

	Game game;
	game.start_game();

	endwin();

	return 0;
}
