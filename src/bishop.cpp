#include "bishop.h"

Bishop::Bishop(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = BISHOP;
    character = 'B';
}

bool Bishop::legal_move(int new_x, int new_y){
    bool legal = false;
  
    if(abs(new_x - x) == abs(new_y - y)){
        legal = true;
    }

    return legal;
}