#include "pawn.h"
#include "game.h"

Pawn::Pawn(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = PAWN;
    character = 'P';
    has_moved = false;
    en_passant = false;
}

bool Pawn::legal_move(int new_x, int new_y){
    bool legal = false; 

    if(board->get_piece(new_x, new_y) == -1){
        if(new_x == x){
            if(color == BLACK){
                if(new_y - 1 == y) legal = true;
                if(new_y - 2 == y && !has_moved) legal = true;
            }
            if(color == WHITE){
                if(new_y + 1 == y) legal = true;
                if(new_y + 2 == y && !has_moved) legal = true;
            }
        }
    }

    if(legal) has_moved = true;

    return legal;
}