#include "knight.h"

Knight::Knight(int x, int y, Color color){
    this->x = x;
    this->y = y;
    this->color = color;
    type = KNIGHT;
    character = 'N';
}

bool Knight::legal_move(int new_x, int new_y){
    bool legal = false;

    if(abs(new_x - x) == 2){
        if(abs(new_y - y) == 1){
            legal = true;
        }
    }

    if(abs(new_y - y) == 2){
        if(abs(new_x - x) == 1){
            legal = true;
        }
    }

    return legal;
}