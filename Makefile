CC = g++
OBJS = ./src/*.cpp
LINK_INCLUDES = -I./include/
LINKER_FLAGS = -lncurses
OBJ_NAME = nchess
all:
	$(CC) $(OBJS) $(LINK_INCLUDES) $(LINKER_FLAGS) -o $(OBJ_NAME)
debug:
	$(CC) -g $(OBJS) $(LINK_INCLUDES) $(LINKER_FLAGS) -o $(OBJ_NAME)
clean:
	$(RM) count *.o *~ $(OBJ_NAME)
