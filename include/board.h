#pragma once
#include <vector>
#include "piece.h"

class Board {
public:
    Board();
    std::vector<Piece*> pieces;
    void draw_board();
    int get_piece(int x, int y);
};