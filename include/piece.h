#pragma once
#include <iostream>

enum Piece_type {
    PAWN,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
    KING
};

enum Color {
    BLACK,
    WHITE,
};

class Piece {
protected:
    int x;
    int y;
    char character;
    Color color;
    Piece_type type;
public:
    virtual bool legal_move(int new_x, int new_y){ return false; }
    void move(int new_x, int new_y);
    void draw();
    int get_x();
    int get_y();
    int get_color();
    int get_type();
};