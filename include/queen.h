#pragma once
#include "piece.h"

class Queen : public Piece {
public:
    Queen(int x, int y, Color color);
    bool legal_move(int new_x, int new_y) override;
};