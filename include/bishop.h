#pragma once
#include "piece.h"

class Bishop : public Piece {
public:
    Bishop(int x, int y, Color color);
    bool legal_move(int new_x, int new_y) override;
};