#pragma once
#include "piece.h"

class Pawn : public Piece {
public:
    Pawn(int x, int y, Color color);
    bool legal_move(int new_x, int new_y) override;
    bool has_moved;
    bool en_passant;
};