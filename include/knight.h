#pragma once
#include "piece.h"

class Knight : public Piece {
public:
    Knight(int x, int y, Color color);
    bool legal_move(int new_x, int new_y) override;
};