#pragma once
#include "piece.h"

class Rook : public Piece {
public:
    Rook(int x, int y, Color color);
    bool legal_move(int new_x, int new_y) override;
    bool has_moved;
};