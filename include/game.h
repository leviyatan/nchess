#pragma once
#include "board.h"

extern Board* board;

class Game {
private:
    void enter_move();
    void parse_move(char* str);
    
public:
    Game();
    void start_game();
};